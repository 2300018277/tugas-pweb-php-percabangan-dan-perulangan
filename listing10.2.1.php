<?php
// Fungsi untuk mengonversi nilai angka ke nilai huruf berdasarkan PAP
function konversiNilai($nilai) {
    if ($nilai >= 85 && $nilai <= 100) {
        return 'A';
    } elseif ($nilai >= 80 && $nilai < 85) {
        return 'A-';
    } elseif ($nilai >= 75 && $nilai < 80) {
        return 'B+';
    } elseif ($nilai >= 70 && $nilai < 75) {
        return 'B';
    } elseif ($nilai >= 65 && $nilai < 70) {
        return 'C+';
    } elseif ($nilai >= 60 && $nilai < 65) {
        return 'C';
    } elseif ($nilai >= 55 && $nilai < 60) {
        return 'D+';
    } elseif ($nilai >= 50 && $nilai < 55) {
        return 'D';
    } else {
        return 'E';
    }
}

// Meminta input nilai dari keyboard (CLI) atau form (WEB)
if (php_sapi_name() == "cli") {
    // Input dari command line interface
    echo "Masukkan nilai anda: ";
    $nilai = trim(fgets(STDIN));
} else {
    // Input dari form HTML
    if (isset($_POST['nilai'])) {
        $nilai = $_POST['nilai'];
    } else {
        $nilai = 0;
    }
}

// Validasi input nilai
if (!is_numeric($nilai) || $nilai < 0 || $nilai > 100) {
    echo "Nilai yang dimasukkan tidak valid. Masukkan nilai antara 0-100.";
} else {
    // Konversi nilai angka ke nilai huruf
    $nilaiHuruf = konversiNilai($nilai);
    echo "Nilai anda $nilai, konversi nilai huruf: $nilaiHuruf";
}
?>

<!-- Form HTML untuk input nilai (untuk penggunaan di web) -->
<!DOCTYPE html>
<html>
<head>
    <title>Konversi Nilai</title>
</head>
<body>
    <form method="post">
        <label for="nilai">Masukkan Nilai Anda:</label>
        <input type="text" id="nilai" name="nilai">
        <input type="submit" value="Konversi">
    </form>
</body>
</html>
